package de.hsb.softw2muebase.controller;

import de.hsb.softw2muebase.model.Movie;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class MovieController {

    /*@GetMapping("/greeting/{name}")
    public String greeting(@PathVariable("name") String name) {
        if (name.length() % 2 == 0) {
            name = "wuensche ich Ihnen, " + name;
        }
        return "Guten Tag " + name;
    }*/

    @GetMapping("/movies/{idOrName}")
    public ResponseEntity get(@PathVariable("idOrName") String idOrName) {
        try {
            var movie = MovieService.getInstance().get(Integer.parseInt(idOrName));
            return new ResponseEntity(movie, movie == null ? HttpStatus.NOT_FOUND : HttpStatus.OK);
        } catch (NumberFormatException nfe) {
            var movies = MovieService.getInstance().get(idOrName);
            return new ResponseEntity(movies, HttpStatus.OK);
        }
    }

    @GetMapping("/movies/")
    public ResponseEntity getAll() {
        return new ResponseEntity(MovieService.getInstance().getAll(), HttpStatus.OK);
    }

    @PostMapping("/movies/")
    public ResponseEntity add(@RequestBody Movie movie) {
        if (movie.getName() == null)
            return new ResponseEntity(null, HttpStatus.BAD_REQUEST);

        MovieService.getInstance().add(movie);
        return new ResponseEntity(null, HttpStatus.CREATED);
    }
}
