package de.hsb.softw2muebase.controller;


import de.hsb.softw2muebase.model.Movies;

public class MovieService {
    private static Movies instance = new Movies();

    private MovieService(){}

    public static Movies getInstance() {
        return instance;
    }

    public static void RESET_MOVIE_SERVICE_TEST_ONLY() {
        instance = new Movies();
    }
}
