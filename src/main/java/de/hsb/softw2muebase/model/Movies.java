package de.hsb.softw2muebase.model;

import java.util.ArrayList;
import java.util.Collection;

public class Movies {

    private final ArrayList<Movie> movies;

    private int index = 0;

    public Movies() {
        this.movies = new ArrayList<>();
    }

    public void add(Movie movie) {
        movie.setID(index++);
        this.movies.add(movie);
    }

    public Movie get(int id) {
        if (id >= index)
            return null;
        return this.movies.get(id);
    }

    public Collection<Movie> get(String name) {
        return this.movies.stream().filter(movie -> movie.getName().equals(name)).toList();
    }

    public Collection<Movie> getAll() {
        return this.movies;
    }

    /*@Override
    public String toString() {
        return "Movies=" + this.movies;
    }*/
}
