package de.hsb.softw2muebase.model;

import java.util.Objects;

public class Movie {

    private Integer id;

    private final String name;

    /*public Movie() {
        this.name = null;
        this.id = null;
    }*/

    public Movie(String name, Integer id) {
        this.name = name;
        this.id = id;
    }

    public Integer getID() {
        return this.id;
    }

    public void setID(Integer id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    /*public void setName(String name) {
        this.name = name;
    }*/

    @Override
    public boolean equals(Object obj) {
        if (obj == this)
            return true;

        if (!(obj instanceof Movie))
            return false;

        final Movie other = (Movie) obj;

        if ((!Objects.equals(this.id, other.id)) || (!Objects.equals(this.name, other.name)))
            return false;

        return true;
    }


    /*@Override
    public String toString() {
        return "Movie{" + "id=" + this.id + ", name=" + this.name + '}';
    }*/

}
